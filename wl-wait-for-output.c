/*
 * Copyright © 2020 Collabora Ltd
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

/* A wayland client that waits for a valid wl_output to be present and then
 * exits. */

#include <errno.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <wayland-client.h>

struct display {
    struct wl_display *wl_display;
    struct wl_registry *wl_registry;
    struct wl_list output_list;
    bool needs_roundtrip;
};

struct output {
    struct wl_list link;
    struct wl_output *wl_output;
    uint32_t server_id;
    bool has_active_mode;
};

static void
output_handle_geometry(void *data, struct wl_output *wl_output,
                       int32_t x, int32_t y,
                       int32_t physical_width, int32_t physical_height,
                       int32_t subpixel,
                       const char *make, const char *model,
                       int32_t output_transform)
{
}

static void
output_handle_mode(void *data, struct wl_output *wl_output,
                   uint32_t flags, int32_t width, int32_t height,
                   int32_t refresh)
{
    struct output *output = data;

    if ((flags & WL_OUTPUT_MODE_CURRENT) && width != 0 && height != 0) {
        output->has_active_mode = true;
        printf("Found output with current mode %dx%d\n", width, height);
    }
}

static void
output_handle_done(void *data, struct wl_output *wl_output)
{
}

static void
output_handle_scale(void *data, struct wl_output *wl_output,
                    int32_t scale)
{
}

static const struct wl_output_listener output_listener = {
    output_handle_geometry,
    output_handle_mode,
    output_handle_done,
    output_handle_scale,
};

static struct output *
output_create(struct wl_output *wl_output, uint32_t id)
{
    struct output *output = calloc(1, sizeof *output);

    output->wl_output = wl_output;
    output->server_id = id;
    wl_output_add_listener(output->wl_output, &output_listener, output);
    wl_list_init(&output->link);

    return output;
}

static void
output_destroy(struct output *output)
{
    wl_list_remove(&output->link);
    wl_output_destroy(output->wl_output);
    free(output);
}

static void
global_handler(void *data, struct wl_registry *registry, uint32_t id,
           const char *interface, uint32_t version)
{
    struct display *display = data;

    if (!strcmp(interface, "wl_output")) {
        struct wl_output *wl_output =
            wl_registry_bind(registry, id, &wl_output_interface, 2);
        struct output *output = output_create(wl_output, id);
        wl_list_insert(&display->output_list, &output->link);
        /* We need a roundtrip to get output information. */
        display->needs_roundtrip = true;
    }
}

static void
global_remove_handler(void *data, struct wl_registry *registry, uint32_t name)
{
    struct display *display = data;
    struct output *output, *tmp;

    wl_list_for_each_safe(output, tmp, &display->output_list, link) {
        if (name == output->server_id)
            output_destroy(output);
    }
}

static const struct wl_registry_listener registry_listener = {
    global_handler,
    global_remove_handler
};

static struct display *
display_create()
{
    struct display *display = calloc(1, sizeof *display);

    display->wl_display = wl_display_connect(NULL);
    if (!display->wl_display) {
        fprintf(stderr, "Failed to connect to wayland server: %s\n",
                strerror(errno));
        free(display);
        return NULL;
    }

    display->wl_registry = wl_display_get_registry(display->wl_display);
    wl_registry_add_listener(display->wl_registry, &registry_listener, display);

    wl_list_init(&display->output_list);

    /* We need a roundtrip to get our first globals. */
    display->needs_roundtrip = true;

    return display;
}

static void
display_destroy(struct display *display)
{
    struct output *output, *tmp;

    wl_list_for_each_safe(output, tmp, &display->output_list, link) {
        output_destroy(output);
    }

    wl_registry_destroy(display->wl_registry);
    wl_display_disconnect(display->wl_display);

    free(display);
}

static bool
display_has_active_mode(struct display *display)
{
    struct output *output;

    wl_list_for_each(output, &display->output_list, link) {
        if (output->has_active_mode)
            return true;
    }

    return false;
}

int
main(int argc, char **argv)
{
    struct display *display = display_create();
    if (!display)
        return 1;

    while (!display_has_active_mode(display)) {
        if (display->needs_roundtrip) {
            display->needs_roundtrip = false;
            wl_display_roundtrip(display->wl_display);
        } else {
            wl_display_dispatch(display->wl_display);
        }
    }

    display_destroy(display);

    return 0;
}
